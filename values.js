//values
function values(obj) {
    let result=[];
    for(let property in obj){
        //if value is a function it will skip
        if((typeof obj[property])==="function"){
            continue
        }
        //adding values
        result.push(`${obj[property]}`)
    }
    return result;
}

module.exports=values;