
//defaults
function defaults(obj, defaultProps){
    
    for(let prop in defaultProps){
        let find=false;
        for(let property in obj){
            if(prop===property){
                find=true
            }
        } 
        if(find ===false){
            obj[prop]=defaultProps[prop];
        } 
    }
    return obj;
}
module.exports=defaults;