
function mapObject(obj, cb) {
  let robj={};
  for(let property in obj){
      //passing each values to cb
      //and saving new value in robj
      robj[property]=cb(obj[property]);
  }
  return robj;
}
module.exports=mapObject;