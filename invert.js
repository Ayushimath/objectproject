//invert

function invert(obj) {
    
    let result={};
    for(let property in obj){
       result[obj[property]]=property;
    }
    return result;
}
module.exports=invert;